package models.actionaudit;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.db.jpa.Model;

/**
 * This class provides the POJO for the Audit Record Database Table
 */
@Entity
public class AuditRecord extends Model {
	/** Serial ID */
	private static final long serialVersionUID = 1L;

	/** The Date the action occurred */
	@Temporal(TemporalType.DATE)
	public Date actionDate;

	/** The current time the action occurred in milliseconds */
	public Long actionTime;

	/** The user that performed the action */
	public String username;

	/** The actual action being performed on the Controller class */
	public String action;

	/** The String description of the action being performed */
	public String message;
}
