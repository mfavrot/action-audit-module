package play.modules.actionaudit;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This class defines the Action Audit Annotation
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ActionAudit {

	/**
	 * The optional detail message to add to the Audit Record when being
	 * recorded.
	 */
	String detailMessage() default "";
}
