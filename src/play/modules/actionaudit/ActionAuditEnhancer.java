package play.modules.actionaudit;


import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.MemberValue;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.classloading.enhancers.Enhancer;

/**
 * This class handles the enhancement of Controller Classes that have the
 * ActionAudit Annotation
 */
public class ActionAuditEnhancer extends Enhancer {

	/**
	 * This method will evaluate the provided Class and determine if any of the
	 * methods are annotated with the ActionAudit Annotation. If there are
	 * methods with the annotation then a code segment will be prepended to the
	 * method body that will insert a record into an Audit table
	 * 
	 * @param appClass - The ApplicationClass object to evaluate
	 * @throws Exception - Throws exception for unexpected errors
	 */
	@Override
	public void enhanceThisClass(ApplicationClass appClass) throws Exception {
		// Obtain the CtClass object to inspect
		CtClass ctClass = makeClass(appClass);

		// Only enhance controller classes.
		if (!ctClass.subtypeOf(classPool.get("play.mvc.Controller"))) {
			return;
		}

		// Find audit annotation on methods
		for (CtMethod ctMethod : ctClass.getMethods()) {
			if (hasAnnotation(ctMethod, ActionAudit.class.getName())) {
				// Get method name
				String mname = ctMethod.getName();

				// Get the detailMessage attribute value if one is present
				Annotation auditAnnotation = getAnnotations(ctMethod).getAnnotation(ActionAudit.class.getName());
				MemberValue mv = auditAnnotation.getMemberValue("detailMessage");
				String auditMessage = "Request to '" + mname + "'";
				if (mv != null) {
					auditMessage = mv.toString();
				}

				// ClassFile ccFile = ctClass.getClassFile();
				// ConstPool constpool = ccFile.getConstPool();
				//
				// // create the annotation
				// AnnotationsAttribute attr = new
				// AnnotationsAttribute(constpool,
				// AnnotationsAttribute.visibleTag);
				// Annotation annot = new Annotation("play.mvc.Before",
				// constpool);
				//
				// // ArrayMemberValue member = new ArrayMemberValue(constPool);
				// // 113: StringMemberValue[] members = new
				// // StringMemberValue[values.length];
				// // 114: for (int i = 0; i < values.length; i++)
				// // 115: members[i] = new StringMemberValue(values[i],
				// // constPool);
				// // 116: member.setValue(members);
				// // 117: annotation.addMemberValue(name, member);
				// ArrayMemberValue amv = new ArrayMemberValue(constpool);
				// StringMemberValue[] vals = new StringMemberValue[1];
				// vals[0] = new StringMemberValue(ctMethod.getName(),
				// constpool);
				// amv.setValue(vals);
				//
				// Logger.debug("adding annotation Before");
				// annot.addMemberValue("only", amv);
				// attr.addAnnotation(annot);
				//
				// // Create the code
				// String code = "";
				// // code = code.concat("public static void " +
				// ctMethod.getName()
				// // + "_ActionAudit(String test) {");
				// code = code.concat("{");
				// code = code.concat("    play.Logger.debug(\"Entering - " +
				// ctMethod.getName() + "\", (Object[])null);");
				// code =
				// code.concat("    String username = session.get(\"username\");");
				// code =
				// code.concat("    models.actionaudit.AuditRecord al = new models.actionaudit.AuditRecord();");
				// code =
				// code.concat("    al.username = session.get(\"username\");");
				// code = code.concat("    al.action = \"" + ctMethod.getName()
				// + "\";");
				// code = code.concat("    al.message = " + auditMessage + ";");
				// code = code.concat("    al.save();");
				// code = code.concat("}");
				//
				// Logger.debug("Injecting...\n" + code + "\n", null);
				// // CtMethod audit = CtNewMethod.make(code, ctClass);
				// String methodName = ctMethod.getName() + "_ActionAudit";
				// CtMethod audit = CtNewMethod.make(Modifier.STATIC,
				// CtClass.voidType, methodName, null, null, code, ctClass);
				// audit.getMethodInfo().addAttribute(attr);
				// ctClass.addMethod(audit);

				// Generate the code block that will handle insertion of the
				// audit record
				StringBuffer code = new StringBuffer();
				code.append("{\n");
				code.append("    play.Logger.debug(\"Entering - " + mname + "\", (Object[])null);\n");
				code.append("    java.lang.String username = session.get(\"username\");\n");
				code.append("    models.actionaudit.AuditRecord al = new models.actionaudit.AuditRecord();\n");
				code.append("    al.username = session.get(\"username\");\n");
				code.append("    al.action = \"" + mname + "\";\n");
				code.append("    Long currTime = new Long(System.currentTimeMillis());\n");
				code.append("    al.actionTime = currTime;\n");
				code.append("    al.actionDate = new java.util.Date(currTime.longValue());\n");
				code.append("    al.message = " + auditMessage + ";\n");
				code.append("    al.save();}");

				// Insert the new code into the method
				ctMethod.insertBefore(code.toString());
			}
		}

		// Done - update class.
		appClass.enhancedByteCode = ctClass.toBytecode();
		ctClass.defrost();
	}
}
