package play.modules.actionaudit;

import play.PlayPlugin;
import play.classloading.ApplicationClasses.ApplicationClass;

/**
 * This class is the main entry point for the Action Audit Plugin/Module. It is
 * responsible for initiating the enhancer code that will process the
 * ActionAudit Annotations
 */
public class ActionAuditPlugin extends PlayPlugin {

	/**
	 * This method is responsible for enhancing Classes for the Action Audit
	 * Plugin/Module
	 * 
	 * @param applicationClass - The ApplicationClass object to evaluate
	 * @throws Exception - Throws exception for unexpected errors
	 */
	@Override
	public void enhance(ApplicationClass applicationClass) throws Exception {
		new ActionAuditEnhancer().enhanceThisClass(applicationClass);
	}

}
